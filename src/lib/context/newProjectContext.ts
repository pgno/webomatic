import { writable, type Writable } from "svelte/store";
import { getContext, setContext } from 'svelte';

type NewProject = { name : string, nameIsValid: boolean, url: string, urlIsValid : boolean}
type Context = Writable<NewProject>

export function setNewProject() {
	let newProject = writable<NewProject>({name:'',nameIsValid: false,url:'', urlIsValid: false})
	setContext('newProject', newProject)
}

export function getNewProject() {
	return getContext<Context>('newProject')
}
