import { Gitlab } from "@gitbeaker/node"; // All Resources
import { error } from "@sveltejs/kit";

const gitlab_token = "glpat-yQCwSBNsJusVyRTDfong";
const project_id = 53529720;
const branch_name = "main";

const api = new Gitlab({
  token: gitlab_token,
});

const commitFiles = async (projectName, files) => {
  let actions = [];
  let gitFiles = [];
  let tree;

  const projectFiles = files.map((f) => f.path);

  // Get current git files
  try {
    tree = await api.Repositories.tree(project_id, {
      path: projectName,
      recursive: true,
    });
    tree.forEach((item) => {
      if (item.type === "blob") {
        gitFiles.push(item.path);
      }
    });
  } catch (e) {}

  //Compare lists
  const updates = projectFiles.filter((f) => gitFiles.includes(f));
  const creates = projectFiles.filter((f) => !gitFiles.includes(f));
  const deletes = gitFiles.filter((f) => !projectFiles.includes(f));

  //Generate actions list
  for (let i = 0; i < files.length; i++) {
    let action = {
      action: "create",
      filePath: files[i].path,
      content: files[i].content,
    };
    if (updates.includes(files[i].path)) action.action = "update";
    if (creates.includes(files[i].path)) action.action = "create";
    actions.push(action);
  }

  for (let j = 0; j < deletes.length; j++) {
    actions.push({
      action: "delete",
      filePath: deletes[j],
    });
  }

  const backupDate = new Date();

  actions.forEach((action) => {
    console.log(action.action, action.filePath);
  });

  try {
    await api.Commits.create(
      project_id,
      branch_name,
      `Backup ${backupDate.toLocaleDateString(
        "fr-FR"
      )} - ${backupDate.getHours()}:${backupDate.getMinutes()}`,
      actions
    );
  } catch (err) {
    throw error(50, {
      title: `Commit error`,
      url: url,
      code: err?.code ?? "UNKNOWN",
      message: err.message || "Unexpected server error",
    });
  }
};

export { commitFiles };
