import { error } from "@sveltejs/kit"
import fm from 'front-matter';
import MarkdownIt from 'markdown-it';
import { imageSize, taskLists, toc } from '@hedgedoc/markdown-it-plugins'
import MarkdownItContainer from 'markdown-it-container'
import MarkdownItTextualUml from 'markdown-it-textual-uml'
import MarkdownItFootnote from 'markdown-it-footnote'

const validateUrl = (url: string) => {
  try {
      return new URL(url)
  }catch (err: unknown) {
      throw error(409, { 
          title: `This is not a valid url`,
          url: url, 
          code: err?.code ?? 'UNKNOWN',
          message : err.message || 'Unexpected server error',
      });
  }
}

const fetchUrl = async (url: string) => {

  if(url.endsWith('?both') || url.endsWith('?view') || url.endsWith('?edit')) 
      url = url.slice(0, -5);
  if(url.endsWith('#') || url.endsWith('?')) 
      url = url.slice(0, -1);

  try {
      const data = await fetch(`${url}/download`)
      const content = await data.text();
      return content;
  }catch (err: unknown) {
      throw error(404, { 
          title: `This url is not reachable`,
          url: url,
          code: err?.code ?? 'UNKNOWN',
          message : err.message || 'Unexpected server error',
      });
  }
}

const extractContent = (_rawContent : string, _url : URL | null)=> {
  try {
      const data = fm(_rawContent);
      return { metadata : data.attributes as Metadata, markdown : data.body }
  }catch (err: unknown) {
      throw error(500, { 
          title: `Can not extract front matter and markdown`,
          url: _url.href,
          code: err?.code ?? 'UNKNOWN',
          message : err.message || 'Unexpected server error',
      });
  }
}

const renderMarkdown= (markdown: string) => {
  const md = new MarkdownIt()
    .set({ html: true, linkify: true, typographer: true})
    .use(imageSize)
    .use(taskLists)
    .use(toc)
    .use(MarkdownItContainer, "info")
    .use(MarkdownItContainer, "warning")
    .use(MarkdownItContainer, "success")
    .use(MarkdownItContainer, "danger")
    .use(MarkdownItTextualUml)
    .use(MarkdownItFootnote)
  const html = md.render(markdown)
  return html;
}

const getSummary = (markdown: string) => {
  const md = new MarkdownIt();
  const tokens = md.parse(markdown,{})

  const summary:Section[] = [];
  const parents = [-1];
  let level = 0;
    
  for (let i = 0; i < tokens.length; i++) {
    
    if (tokens[i].type === "bullet_list_open") {
      parents.push(summary.length - 1);
      level++;
    }
    if (tokens[i].type === "bullet_list_close") {
      parents.pop();
      level--;
    }
    if (tokens[i].type === "inline" && level > 0) {
      const block = tokens[i];
      const section:Section = {
        id : summary.length,
        parent: parents[parents.length - 1],
        level,
        url : null,
        title: 'Undefined',
        page : null,
        path : null
      };

      if(block.children){
        for (let j = 0; j < block.children.length; j++) {
          if (block.children[j].type === "link_open") {
            const attributes = block.children[j].attrs;
            if(attributes) {
              const link = attributes.find((attr) => attr[0] === "href");
              if (link) {
                section.url = link[1];
              }

              if (
                block.children[j + 1] &&
                block.children[j + 1].type === "text"
              ) {
                section.title = block.children[j + 1].content;
                section.path = slugify(section.title)
                j++;
              }
            }
          }
        }
      }
      summary.push(section);
    }
  }
  return summary;
};

const slugify = (str: string) => {
  return String(str)
    .normalize('NFKD') // split accented characters into their base characters and diacritical marks
    .replace(/[\u0300-\u036f]/g, '') // remove all the accents, which happen to be all in the \u03xx UNICODE block.
    .toLowerCase() // convert to lowercase
    .replace(/[^a-z0-9 -]/g, '') // remove non-alphanumeric characters
    .replace(/\s+/g, '-') // replace spaces with hyphens
    .replace(/-+/g, '-') // remove consecutive hyphens
    .trim() // trim leading or trailing whitespace
}

export {validateUrl,fetchUrl, extractContent, getSummary, slugify, renderMarkdown}