import type { TokensList } from "Tokens";
import qrcode from "qrcode";
import ms from "ms";
import { config } from "dotenv";

export const defaultPrintConfig = {
  size: "A5",
  margins: {
    top: 20,
    left: 10,
    right: 10,
    bottom: 20,
  },
  hasLeftRightMargin: false,
  marginsRight: {
    top: 20,
    left: 10,
    right: 10,
    bottom: 20,
  },
  containers: {
    info: {
      backgroundColor: "#d9edf7",
      borderRadius: 10,
      textFont: "",
      textFontColor: "inherit",
      textFontSize: 10,
      textLineHeight: 12,
      textAlign: "left",
      paddingX: 5,
      paddingY: 10,
    },
    success: {
      backgroundColor: "#dff0d8",
      borderRadius: 10,
      textFont: "",
      textFontColor: "inherit",
      textFontSize: 10,
      textLineHeight: 12,
      textAlign: "left",
      paddingX: 5,
      paddingY: 10,
    },
    warning: {
      backgroundColor: "#fcf8e3",
      borderRadius: 10,
      textFont: "",
      textFontColor: "inherit",
      textFontSize: 10,
      textLineHeight: 12,
      textAlign: "left",
      paddingX: 5,
      paddingY: 10,
    },
    danger: {
      backgroundColor: "#f2dede",
      borderRadius: 10,
      textFont: "",
      textFontColor: "inherit",
      textFontSize: 10,
      textLineHeight: 12,
      textAlign: "left",
      paddingX: 5,
      paddingY: 10,
    },
  },
  coverUrl: "",
  headingFont: "",
  headingFontColor: "#000000",
  h1FontSize: 48,
  h2FontSize: 32,
  h3FontSize: 24,
  h4FontSize: 18,
  h5FontSize: 14,
  h6FontSize: 10,
  textFont: "",
  textFontColor: "#000000",
  textFontSize: 12,
  textLineHeight: 16,
  textAlign: "justify",
  pageBreakOnNewChapter: true,
  showPageNumber: true,
  showPageTitleInBottomMargin: true,
  showCoverImage: true,
  hideCoverTitle: true,
  useH6AsPageBreak: false,
};

const containers = ["info", "success", "warning", "danger"];

export function timeAgo(
  timestamp: string | number | Date,
  timeOnly: boolean = false
) {
  if (!timestamp) return "never";
  return `${ms(Date.now() - new Date(timestamp).getTime())}${
    timeOnly ? "" : " ago"
  }`;
}

export function slugify(str: string) {
  return String(str)
    .normalize("NFKD") // split accented characters into their base characters and diacritical marks
    .replace(/[\u0300-\u036f]/g, "") // remove all the accents, which happen to be all in the \u03xx UNICODE block.
    .toLowerCase() // convert to lowercase
    .replace(/[^a-z0-9 -]/g, "") // remove non-alphanumeric characters
    .replace(/\s+/g, "-") // replace spaces with hyphens
    .replace(/-+/g, "-") // remove consecutive hyphens
    .trim(); // trim leading or trailing whitespace
}

export function generateStylesheet(configObject) {
  let styleSheet = "";

  if (configObject.textFont && configObject.textFont != "") {
    styleSheet += `
    /* Import text font */
    @import url('https://fonts.googleapis.com/css2?family=${configObject.textFont.replace(
      " ",
      "+"
    )}&display=swap');
    `;
  }

  if (configObject.headingFont && configObject.headingFont != "") {
    styleSheet += `
    /* Import headings font */
    @import url('https://fonts.googleapis.com/css2?family=${configObject.headingFont.replace(
      " ",
      "+"
    )}&display=swap');
    `;
  }

  styleSheet += `
    /* Create fonts variable */
    :root {
        --heading-font : '${configObject.headingFont || "Roboto Slab"}';
        --heading-font-color : ${configObject.headingFontColor};
        --text-font : '${configObject.textFont || "Lato"}';
        --text-font-color : ${configObject.textFontColor};
        --text-font-size : ${configObject.textFontSize}pt;
        --text-line-height : ${configObject.textLineHeight}pt;
    }

    `;

  if (configObject.hasLeftRightMargin && configObject.marginsRight) {
    styleSheet += `
      /* Set page dimension and fonts family, size and color */
      
      @page {
        size : ${configObject.size};
    }
      @page:left {
          margin: ${configObject.margins.top}mm ${configObject.margins.right}mm ${configObject.margins.bottom}mm ${configObject.margins.left}mm;
      }

      @page:right {
        margin: ${configObject.marginsRight.top}mm ${configObject.marginsRight.right}mm ${configObject.marginsRight.bottom}mm ${configObject.marginsRight.left}mm;
      }
    `;
  } else {
    styleSheet += `
      @page {
          size : ${configObject.size};
          margin: ${configObject.margins.top}mm ${configObject.margins.right}mm ${configObject.margins.bottom}mm ${configObject.margins.left}mm;
      }
    `;
  }

  styleSheet += `
    h1,h2,h3,h4,h5,h6 {
        font-family : var(--heading-font);
        color : var(--heading-font-color);
    }

    h1 { font-size: ${configObject.h1FontSize}pt; line-height:${configObject.h1FontSize}pt; }
    h2 { font-size: ${configObject.h2FontSize}pt; line-height:${configObject.h1FontSize}pt; }
    h3 { font-size: ${configObject.h3FontSize}pt; line-height:${configObject.h1FontSize}pt; }
    h4 { font-size: ${configObject.h4FontSize}pt; line-height:${configObject.h1FontSize}pt; }
    h5 { font-size: ${configObject.h5FontSize}pt; line-height:${configObject.h1FontSize}pt; }
    h6 { font-size: ${configObject.h6FontSize}pt; line-height:${configObject.h1FontSize}pt; }

    h1, h2, h3, h4, h5, h6{
      text-align: left;
      text-align-last: left;
      clear: both 
    }

    .chapter, .chapter p, .chapter li {
        font-family : var(--text-font);
        color : var(--text-font-color);
        font-size : var(--text-font-size);
        line-height: var(--text-line-height);
        text-align: ${configObject.textAlign};
        text-align-last: left;
    }

    .pagedjs_margin {
        font-family : var(--text-font);
        color : var(--text-font-color);
        font-size : var(--text-font-size);
    }
    
    `;

  if (configObject.pageBreakOnNewChapter) {
    styleSheet += `
    /* Break page on new section (.chapter) */
    section {
        break-before: page;
    }
    `;
  }

  if (configObject.showPageNumber) {
    styleSheet += `
    /* Show page number */
    @page:left {
        @bottom-left {
            content: counter(page);
        }
    }

    @page:right {
        @bottom-right {
            content: counter(page);
        }
    }
    `;
  }

  if (configObject.showPageTitleInBottomMargin) {
    styleSheet += `
    /* Display book title in page bottom margin */
    #cover h1 {
        string-set: title content(text);
    }

    @page {
        @bottom-center {
            content: string(title);
        }
    }
    `;
  }

  if (
    configObject.showCoverImage &&
    configObject.coverUrl &&
    configObject.coverUrl !== ""
  ) {
    styleSheet += `
    /* Show Cover image */
    section#cover {
      page: cover;
    }
    @page cover {
        background-image: url("${configObject.coverUrl}");
        background-size: cover;
      }
    `;
  }

  if (configObject.hideCoverTitle) {
    styleSheet += `
    /* Hide title on Cover page */
    #cover h1 {
        display : none;
    }
    `;
  } else {
    styleSheet += `
    /* Show title on Cover page */
    #cover h1 {
      background-color: white;
      position: absolute;
      line-height: 2rem;
      padding: 2rem;
      width: 100%;
      right: calc(var(--pagedjs-margin-right) * -1);
      bottom: calc((var(--pagedjs-margin-bottom) * -1));
    }

    `;
  }

  if (configObject.useH6AsPageBreak) {
    styleSheet += `
    /* h6 tag is hidden and generate a page break */
    .chapter h6 {
        page-break-after: always;
    }
    `;
  }

  styleSheet += `
    /* All images will be full width + little markdown hack to float image */
    img {
      max-width : 100%;
    }
    img[src*='#left'] {
      float: left;
      margin-right : 7mm;
    }
    img[src*='#right'] {
        float: right;
        margin-left : 7mm;
    }
    img[src*='#center'] {
        display: block;
        margin: auto;
    }

    img[src*='#fullpage'] {
      position: absolute;
      width: var(--pagedjs-width);
      max-width: var(--pagedjs-width);
      height: var(--pagedjs-height);
      object-fit: cover;
      top: calc(-1 * var(--pagedjs-margin-top));
      left: calc(-1 * var(--pagedjs-margin-left));
    }

    `;
  styleSheet += `/* Container Style */`;
  containers.forEach((containerType) => {
    const containerStyle = configObject.containers[containerType];
    if (containerStyle.textFont && containerStyle.textFont != "") {
      styleSheet += `
      /* Import text font */
      @import url('https://fonts.googleapis.com/css2?family=${containerStyle.textFont.replace(
        " ",
        "+"
      )}&display=swap');
      `;
    }
    styleSheet += `
      .chapter .${containerType} {
        background-color : ${containerStyle.backgroundColor} !important;
        border-radius : ${containerStyle.borderRadius}pt !important;
        padding : ${containerStyle.paddingY}mm ${
      containerStyle.paddingX
    }mm !important;
        margin-bottom: 8mm !important;
      }
      .chapter .${containerType} p , .chapter .${containerType} li {
         
        font-family : '${containerStyle.textFont || "Lato"}' !important;
        color: ${containerStyle.textFontColor || "inherit"} !important;
        font-size :  ${containerStyle.textFontSize}pt !important;
        line-height: ${containerStyle.textLineHeight}pt !important;
        text-align:  ${containerStyle.textAlign} !important;
        text-align-last: left !important;

      }
      
      `;
  });

  styleSheet += `
    /* Default style for QRCode */
    .qrcode {
      display: flex;
      justify-content: center;
    }
    
    .qrcode img {
      max-width: 20mm;
    }
    `;

  return styleSheet;
}

export const buildTree = (dataset: any[]) => {
  const dataTree: any[] = [];
  if (dataset && dataset.length > 0) {
    const hashTable = Object.create(null);
    dataset.forEach((aData) => {
      hashTable[aData.id] = { ...aData, children: [] };
    });

    dataset.forEach((aData) => {
      if (aData.parent > -1)
        hashTable[aData.parent].children.push(hashTable[aData.id]);
      else dataTree.push(hashTable[aData.id]);
    });
  }

  return dataTree;
};

export const replaceQrCode = (content: string) => {
  const doc = document.createElement("div");
  doc.innerHTML = content;

  const qrcodeLinks = doc.querySelectorAll('a[href*="qrcode"');
  qrcodeLinks.forEach((link) => {
    const qrcodeTag = document.createElement("div");
    qrcodeTag.classList.add("qrcode");
    const img = document.createElement("img");
    qrcodeTag.appendChild(img);
    const href = link.href.substr(0, link.href.lastIndexOf("#qrcode"));

    qrcode.toDataURL(href, { type: "image/jpeg" }, function (error, url) {
      if (error) console.error(error);
      img.src = url;
    });
    const parentElt = link.parentElement;
    if (parentElt) {
      parentElt.appendChild(qrcodeTag);
      link.remove();
    }
  });

  return doc.innerHTML;
};
