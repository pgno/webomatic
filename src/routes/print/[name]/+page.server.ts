import { error } from '@sveltejs/kit';

export async function load({ params, fetch }) {

  const name = params.name;
  let content = '';

  
  const data = await fetch(`/api/projects/view/${name}`);
  const response = await data.json();
  const project = response.project;
  if( data.status !== 200) {
    throw error(404,'Site not found');
  }


  if(project.data.metadata && (project.data.metadata.title || project.data.metadata.cover)) 
  content += '<section id="cover"><h1>'+project.data.metadata.title+'</h1></section>';
  
  for (const section of project.data.sections) {
    content += `<section class="chapter">${section.page.content}</section>`
  }

  return {
    project,
    content
  };
}
