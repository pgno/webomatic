import { error } from '@sveltejs/kit';
export async function load({ params,fetch }) {

  const name = params.name;
  
  const data = await fetch(`/api/projects/view/${name}`);
  const response = await data.json();
  if( data.status !== 200) {
    throw error(404,'Project not found');
  }

  return {
    project : response.project 
  };
}

