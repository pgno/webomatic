import { error, fail } from "@sveltejs/kit";

export async function load({ fetch }) {
  const data = await fetch("/api/projects");
  const response = await data.json();

  const projects = response.sort((a, b) => {
    const date_a: number = new Date(a.created_at).getTime();
    const date_b: number = new Date(b.created_at).getTime();
    return date_b - date_a;
  });

  if (data.status === 200) {
    return {
      projects: response,
    };
  } else {
    throw error(data.status, { message: response.json });
  }
}
