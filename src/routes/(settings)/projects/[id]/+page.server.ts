import { error, fail } from "@sveltejs/kit";

export async function load({params,fetch}) {
  const id = params.id;
  const data = await fetch(`/api/projects/${id}`);
  const response = await data.json();


  if(data.status === 200) {
      return {
        project: response.project
      }
  }else{
    throw error(data.status,{message : response.json});
  }
  
}
