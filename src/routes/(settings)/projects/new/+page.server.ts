import { sql } from "@vercel/postgres";
import type { PageServerLoad } from "../$types";

export const load: PageServerLoad = async () => {
    let names: string[] = []
    const {rows} = await sql`SELECT (data ->> 'name') as name FROM projects`;
    if(rows) names = rows.map( r=> r.name );
    return {
        names
    }
};

