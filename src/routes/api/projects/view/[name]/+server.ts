import { error, json, type RequestHandler } from "@sveltejs/kit";
import { sql } from '@vercel/postgres';

export const GET: RequestHandler = async ( { params } ) => {
    const name = params.name;
    
    try {
        const query = '{"name": "'+name+'"}'
        const { rows } = await sql`SELECT * FROM projects WHERE data @> ${query}`;
         return json({
            project : rows[0]
        });
    }catch(err){
        throw error(500, { 
          title: `Project with ${name} not found in database`,
          code: err?.code ?? 'UNKNOWN',
          message : err.message || 'Unexpected server error',
      });
    }

   
};