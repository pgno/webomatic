import * as fs from "fs";
import { error, json, type RequestHandler } from "@sveltejs/kit";
import {
  extractContent,
  fetchUrl,
  getSummary,
  validateUrl,
} from "$lib/server/content";
import MarkdownIt from "markdown-it";
import { sql } from "@vercel/postgres";

const md = new MarkdownIt();

export const GET: RequestHandler = async () => {
  try {
    const { rows } =
      await sql`SELECT id,created_at, updated_at, data #>> '{name}' as name, data #>> '{metadata,title}' as title, data #>> '{metadata,cover}' as cover, data #>> '{metadata,author}' as author, data #>> '{metadata,tags}' as tags, data #>> '{metadata,collection}' as collection  FROM projects`;
    return json(rows);
  } catch (err) {
    throw error(500, {
      title: `Can not retrieve projects from database`,
      code: err?.code ?? "UNKNOWN",
      message: err.message || "Unexpected server error",
    });
  }
};
