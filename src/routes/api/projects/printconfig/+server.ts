import { error, json, type RequestHandler } from "@sveltejs/kit";
import { sql } from '@vercel/postgres';


export const POST: RequestHandler = async ( { request } ) => {
    const { id,content } = await request.json()

    if(!id || id === '' || typeof id !== 'number') {
        throw error(409, {
            title : 'You must provid a valid id',
            code: 'ID_NOT_FOUND',
            message: 'Id required'
        })
    }


    try {
        
        await sql`UPDATE projects SET print_config = ${content} WHERE id=${id}`;
        
    }catch(err) {
        throw error(500, { 
        title: `Can not update printconfig to database`,
        code: err?.code ?? 'UNKNOWN',
        message : err.message || 'Unexpected server error',
        });
    }
    
    return json({success : true});
};