import { error, json, type RequestHandler } from "@sveltejs/kit";
import { sql } from '@vercel/postgres';

export const GET: RequestHandler = async ( { params } ) => {
    const id = params.id;
    try {
        const { rows } = await sql`SELECT * FROM projects WHERE id=${id}`;
         return json({
            project : rows[0]
        });
    }catch(err){
        throw error(500, { 
          title: `Project with ${id} not found in database`,
          code: err?.code ?? 'UNKNOWN',
          message : err.message || 'Unexpected server error',
      });
    }

   
};