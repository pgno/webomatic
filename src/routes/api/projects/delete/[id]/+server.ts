import { error, json, type RequestHandler } from "@sveltejs/kit";
import { sql } from '@vercel/postgres';


export const POST: RequestHandler = async ( { request } ) => {
    const { id } = await request.json()

    if(!id || id === '' || typeof id !== 'number') {
        throw error(409, {
            title : 'You must provid a valid id',
            code: 'ID_NOT_FOUND',
            message: 'Id required'
        })
    }
    try {
        await sql`DELETE FROM projects WHERE id=${id}`;
    }catch(err) {
        throw error(500, { 
        title: `Can not delete project from database`,
        code: err?.code ?? 'UNKNOWN',
        message : err.message || 'Unexpected server error',
        });
    }
    
    return json({success : true});
};