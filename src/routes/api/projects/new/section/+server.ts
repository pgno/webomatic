import { error, json, type RequestHandler } from "@sveltejs/kit";
import { extractContent, fetchUrl, getSummary, renderMarkdown, validateUrl } from "$lib/server/content";

import { sql } from '@vercel/postgres';


export const POST: RequestHandler = async ( { request } ) => {
    const { projectId,sectionId,url } = await request.json()

    const _url = validateUrl(url)
    const _rawContent = await fetchUrl(_url.toString())
    const { metadata, markdown } = extractContent(_rawContent,_url)

    const page:Page = {
        content: renderMarkdown(markdown),
        metadata,
    }

    
    if(typeof projectId !== 'undefined' && typeof sectionId !== 'undefined') {
        try {
            const path = '{sections,'+sectionId+',page}';
            const content = `"${markdown}"`;
            await sql`UPDATE projects SET data = jsonb_set(data, ${path}, ${JSON.stringify(page)}::jsonb) WHERE id=${projectId}`   
            
        }catch(err) {
            throw error(500, { 
            title: `Can not update project to database`,
            url: url,
            code: err?.code ?? 'UNKNOWN',
            message : err.message || 'Unexpected server error',
            });
        }
    }

    return json({ success : "ok" });
};