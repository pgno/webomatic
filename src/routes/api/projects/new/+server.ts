import { error, json, type RequestHandler } from "@sveltejs/kit";
import { extractContent, fetchUrl, getSummary, renderMarkdown, validateUrl } from "$lib/server/content";
import { sql } from '@vercel/postgres';

export const POST: RequestHandler = async ( { request } ) => {
    const { id,name,url } = await request.json()
    const errors = [];

    const _url = validateUrl(url)
    const _rawContent = await fetchUrl(_url.toString())
    const { metadata, markdown } = extractContent(_rawContent,_url)
    const sections = getSummary(markdown);

    for (let i=0; i<sections.length; i++) {
        const section = sections[i] 
        if(section.url){
            try {
                const __url = validateUrl(section.url)
                const __rawContent = await fetchUrl(__url.toString())
                const { metadata, markdown } = extractContent(__rawContent,__url)
                const content = renderMarkdown(markdown)
                section.page = {
                    metadata,
                    content,
                };
                
            }catch (err) {
                errors.push({
                    url : section.url,
                    title : err.body.title || 'Undefined',
                    code : err.body.code,
                    message : err.body.message
                })
            }
        }
    }

    if(errors.length>0) {
        throw error(500, { 
          title: `Problem while parsing your document`,
          url: url,
          code: 'INVALID_SUMMARY',
          message : 'One or more of your urls are not valid',
          errors
      });
    }

    const project:Project = {
        name,
		url,
		metadata,
		sections,
    }

    if(id) {
        try {
            await sql`UPDATE projects SET data = ${JSON.stringify(project)}, updated_at = CURRENT_TIMESTAMP WHERE id=${id}`;
        }catch(err) {
            throw error(500, { 
            title: `Can not update project to database`,
            url: url,
            code: err?.code ?? 'UNKNOWN',
            message : err.message || 'Unexpected server error',
            });
        }
    }else{
        try {
        const { rows } = await sql`INSERT INTO projects (data)
            VALUES (${JSON.stringify(project)}) RETURNING id`;
            //project.id = rows[0].id;
        }catch(err){
            throw error(500, { 
            title: `Can not save project to database`,
            url: url,
            code: err?.code ?? 'UNKNOWN',
            message : err.message || 'Unexpected server error',
        });
        }
    }

    return json({
        project,
        errors
    });
};