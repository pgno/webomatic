import { error, json, type RequestHandler } from "@sveltejs/kit";
import { extractContent, fetchUrl, getSummary, validateUrl } from "$lib/server/content";
import { sql } from '@vercel/postgres';




export const POST: RequestHandler = async ( { request } ) => {
    const { id,url } = await request.json()

    const _url = validateUrl(url)
    const _rawContent = await fetchUrl(_url.toString())
    const { metadata, markdown } = extractContent(_rawContent,_url)

    if(id) {
        try {
            await sql`UPDATE projects SET data = jsonb_set(data, '{metadata}', ${metadata}::jsonb) where id=${id}`
            //await sql`UPDATE projects SET data = ${JSON.stringify(project)}, updated_at = CURRENT_TIMESTAMP WHERE id=${id}`;
        }catch(err) {
            throw error(500, { 
            title: `Can not update project to database`,
            url: url,
            code: err?.code ?? 'UNKNOWN',
            message : err.message || 'Unexpected server error',
            });
        }
    }

    return json({ success : "ok" });
};