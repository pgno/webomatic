import { error, json, type RequestHandler } from "@sveltejs/kit";
import { sql } from '@vercel/postgres';


export const POST: RequestHandler = async ( { request } ) => {
    const { id,type,content } = await request.json()

    if(!id || id === '' || typeof id !== 'number') {
        throw error(409, {
            title : 'You must provid a valid id',
            code: 'ID_NOT_FOUND',
            message: 'Id required'
        })
    }

    if(!type || type === '' || typeof type !== 'string' ) {
        throw error(409, {
            title : 'You must provid a css destination (print or screen)',
            code: 'TYPE_NOT_FOUND',
            message: 'Type required'
        })
    }


    try {
        if( type === 'screen' ) {
            await sql`UPDATE projects SET styles_screen = ${content} WHERE id=${id}`;
        }
        if( type === 'print' ) {
            await sql`UPDATE projects SET styles_print = ${content} WHERE id=${id}`;
        }

        
    }catch(err) {
        throw error(500, { 
        title: `Can not update style to database`,
        code: err?.code ?? 'UNKNOWN',
        message : err.message || 'Unexpected server error',
        });
    }
    
    return json({success : true});
};