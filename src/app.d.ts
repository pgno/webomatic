// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
declare global {
  namespace App {
    // interface Error {}
    // interface Locals {}
    // interface PageData {}
    // interface Platform {}
  }

  type ReportMessage = {
    message: string;
    title: string;
    code: string;
    url: string;
  };

  type Metadata = {
  		[key: string]: unknown;
	}

	type Page = {
		content : string | null;
		metadata : Metadata | null;
	}

	type Section = {
		id: number;
		title: string;
		url: string | null;
		level: number;
		parent: number;
    path: string | null,
		page: Page | null;
	}

	type Project = {
		name: string;
		url: string;
		metadata: Metadata | null;
		sections : Section[];
	}
}

declare module '@fortawesome/pro-solid-svg-icons/index.es' {
  export * from '@fortawesome/pro-solid-svg-icons';
}

export {}
